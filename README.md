# UnityTest

Project structure:
============
Scene: SampleScene

MainCamera

Amazon

-Canvas

--Text

--Stamina

--Aura

---Particle System

MainCanvas

-ListButton

--AttackButton

--DeathButton

--PainButton

Trees

AmazonCamera

Scenarios:
============
When click Play button you see Amazon prefab with UI which contains Text and Stamina bar. Camera spins around Amazon. Also Amazon contains Aura. Aura get image from native library(FractalGenerator) each 5 seconds. You can change time, name image, Aura material from FractalNativeGeneratorScript on Aura gameobject. Scene contains main UI(MainCanvas): AttackButton, DeathButton, PainButton. 
Each main canvas button contains animation. 

Click:
============
1. AttackButton - set to Aura particle system UnlitTransparentShader. Alpha mask get from native shader(FractalGenerator)

2. DeathButton - start restore Stamina because this action take off all stamina.

3. PainButton - start post processing effect(Blur). All gameobjects with PostProcessing layer use this effect. Amazon has different layer(Amazon).

Stamina bar:
============
Full Stamina bar - 1000. Each buttons from Main Canvas have weight stamina which can change from Cost field. If stamina equal 0 stamina will fill up immediately. You can do some actions if enough stamina value. After interrupt stamina stop fill up. After 10 seconds inaction stamina will fill up. You can change time from StaminaControllerScript.IdleTimeToRestore in Amazon.Canvas.Stamina.