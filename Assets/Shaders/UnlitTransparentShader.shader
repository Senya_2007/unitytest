﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/UnlitTransparentShader"
{
  Properties
   {
      _MainTex ("Base (RGB)", 2D) = "white" {}
      _Mask ("Alpha", 2D) = "white" {}
   }
   SubShader
   {
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
     Pass {

            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
 
            #include "UnityCG.cginc"
           
            uniform sampler2D _MainTex;
			uniform sampler2D _Mask;
 
            fixed4 frag(v2f_img i) : SV_Target {
                fixed4 r = tex2D(_MainTex, i.uv);
				fixed4 colorAlpha = tex2D(_Mask, i.uv);
               
                    r.a = colorAlpha.r;
					
                
				return r;
            }
            ENDCG
        }
   }
}
