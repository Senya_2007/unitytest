﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraControllerScript : MonoBehaviour
{
    public GameObject AmazonCamera;
    public GameObject MainCamera;
    // Start is called before the first frame update
    void Start()
    {
        AmazonCamera.SetActive(true);
    }

    public void ChangePostProcessingEffect()
    {
        StartCoroutine(ShowPostProcessingEffect());
    }

    IEnumerator ShowPostProcessingEffect()
    {
        MainCamera.GetComponent<PostProcessLayer>().enabled = true;
        yield return new WaitForSeconds(1.0f);
        MainCamera.GetComponent<PostProcessLayer>().enabled = false;
    }
}
