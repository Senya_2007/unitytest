﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCameraScript : MonoBehaviour
{
    public Camera MainCamera;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(transform.position + MainCamera.transform.rotation*Vector3.forward, MainCamera.transform.rotation * Vector3.up);
    }
}
