﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaExpenseScript : MonoBehaviour
{
    public int Cost = 0;
    public GameObject StaminaBar;
    public GameObject AuraParticleSystem;
    public Animator AmazonAnimator;
    public string AnimationName;
    

    public void Do()
    {
        int currentStamina = (int)(StaminaBar.GetComponent<Slider>().value * 1000);
        if (currentStamina >= Cost)
        {
            DoSomeEffects();
            AmazonAnimator.Play(AnimationName);
            StaminaBar.GetComponent<StaminaControllerScript>().Reduce(Cost);
        }
    }

    private void DoSomeEffects()
    {
        switch (gameObject.name)
        {
            case "PainButton":
                GetComponent<CameraControllerScript>().ChangePostProcessingEffect();
                break;

            case "AttackButton":
                AuraParticleSystem.GetComponent<SetShaderScript>().ChangeMaterial();
                break;
        }
    }


}
