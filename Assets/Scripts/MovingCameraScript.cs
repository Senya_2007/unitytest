﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCameraScript : MonoBehaviour
{
    public GameObject Target;

    float speedMod = 3.0f;
    Vector3 point;

    void Start()
    {
        point = Target.transform.position;
    }

    void Update()
    {
        transform.RotateAround(point, Vector3.down, 10 * Time.deltaTime * speedMod);
    }
}
