﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaControllerScript : MonoBehaviour
{
    public float IdleTimeToRestore = 10.0f;

    Slider slider;
    Coroutine restoreCoroutine;
    bool startRestoreStamina = true;
    float defaultTimeToRestore;

    void Start()
    {
        defaultTimeToRestore = IdleTimeToRestore;
        slider = GetComponent<Slider>();
        StartCoroutine(ControlZeroStamina());
    }

    void Update()
    {
        IdleTimeToRestore -= Time.deltaTime;

        if (IdleTimeToRestore <= 0.0f && startRestoreStamina)
        {
            TimerEnded();
            startRestoreStamina = false;
        }

    }

    public void Reduce(int stamina)
    {
        if (restoreCoroutine != null)
            StopCoroutine(restoreCoroutine);
        slider.value = GetComponent<Slider>().value - (float)stamina / 1000;

        IdleTimeToRestore = defaultTimeToRestore;
        startRestoreStamina = true;
    }

    private void TimerEnded()
    {
        Debug.Log("Restore stamina");
        if(slider.value != 1)
            restoreCoroutine = StartCoroutine(RestoreStamina());
    }


    IEnumerator ControlZeroStamina()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.0f);
            if (slider.value == 0)
            {
                restoreCoroutine = StartCoroutine(RestoreStamina());
            }
        }
    }


    IEnumerator RestoreStamina()
    {
        do
        {
            yield return new WaitForSeconds(0.2f);
            slider.value += 0.01f;
        } while (slider.value < 1);

        if (slider.value == 1)
            startRestoreStamina = true;
    }
   
}
