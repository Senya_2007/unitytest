﻿using B83.Image.BMP;
using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public class FractalNativeGeneratorScript : MonoBehaviour
{
    [DllImport("FractalGenerator", EntryPoint = "GetImage")]
    public static extern int GetImage(string path, double random, int colorMap);


    public string Filename = "auraTexture.bmp";
    public Material AuraMaterial;
    public float ChangeTime = 5.0f;

    string path = string.Empty;

    void Start()
    {
        path = Application.persistentDataPath+ "/" + Filename;
        StartCoroutine(SetTextureFromNative(path,0,3));
    }

    public Texture2D GetImageFromNative(string filePath, int randStart, int randEnd)
    {
        System.Random rand = new System.Random();

        double randomFractalParts = Convert.ToDouble(rand.Next(100000)) / 100;

        System.Random randColorMap = new System.Random();

        GetImage(filePath, randomFractalParts, randColorMap.Next(randStart, randEnd));

        return LoadTexture(filePath);
    }

    IEnumerator SetTextureFromNative(string filePath, int randStart, int randEnd)
    {
        while (true)
        {
            yield return new WaitForSeconds(ChangeTime);
            System.Random rand = new System.Random();

            double randomFractalParts = Convert.ToDouble(rand.Next(100000)) / 100;

            System.Random randColorMap = new System.Random();

            GetImage(filePath, randomFractalParts, randColorMap.Next(randStart, randEnd));

            AuraMaterial.mainTexture = LoadTexture(filePath);
        }
    }

    public Texture2D LoadTexture(string filePath)
    {
        Texture2D texture = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);

            BMPLoader bmpLoader = new BMPLoader();

            BMPImage bmpImg = bmpLoader.LoadBMP(fileData);

            texture = bmpImg.ToTexture2D();
        }
        return texture;
    }
}
