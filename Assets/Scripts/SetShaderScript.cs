﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetShaderScript : MonoBehaviour
{
    public Material MainMaterial;
    public GameObject FractalGenerator;

    string filename = "alpha.bmp";

    Material currentMaterial;
    // Start is called before the first frame update
    public void ChangeMaterial()
    {
        currentMaterial = GetComponent<Renderer>().material;
        string path = Application.persistentDataPath + "/" + filename;
        var alphaTexture = FractalGenerator.GetComponent<FractalNativeGeneratorScript>().GetImageFromNative(path,4,5);

        MainMaterial.SetTexture("_Mask", alphaTexture);
        GetComponent<Renderer>().material = MainMaterial;
        StartCoroutine(ReturnPreviousMaterial());
    }

    IEnumerator ReturnPreviousMaterial()
    {
        yield return new WaitForSeconds(3.0f);
        GetComponent<Renderer>().material = currentMaterial;
    }
   
}
