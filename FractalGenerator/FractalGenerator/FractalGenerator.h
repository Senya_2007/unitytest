#define FRACTALGEN __declspec(dllexport) 

extern "C" {
	FRACTALGEN int GetImage(const char* file, double random, int colorMap);
}