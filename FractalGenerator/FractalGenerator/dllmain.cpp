﻿// dllmain.cpp : Определяет точку входа для приложения DLL.
#include "pch.h"
#include "FractalGenerator.h"
#include <cmath>
#include "bitmap_image.hpp"
#include <iostream>



extern "C" {
	int APIENTRY GetImage(const char* file, double random, int colorMap)
	{
		bitmap_image fractal(600, 400);

		fractal.clear();

		const unsigned int max_iterations = 300;

		const double cr = -0.70000;
		const double ci = 0.27015;

		double prevr, previ;

		for (unsigned int y = 0; y < fractal.height(); ++y)
		{
			for (unsigned int x = 0; x < fractal.width(); ++x)
			{
				double nextr = 1.5 * (2.0 * x / fractal.width() - 1.0);
				double nexti = (2.0 * y / fractal.height() - 1.0);

				for (unsigned int i = 0; i < max_iterations; ++i)
				{
					prevr = nextr;
					previ = nexti;

					nextr = prevr * prevr - previ * previ + cr;
					nexti = 2 * prevr * previ + ci;

					if (((nextr * nextr) + (nexti * nexti)) > 4)
					{
						rgb_t c = jet_colormap[static_cast<int>((random * i) / max_iterations)];
						switch (colorMap)
						{
						case 0:
							 c = jet_colormap[static_cast<int>((random * i) / max_iterations)];
							 break;
						case 1:
							c = copper_colormap[static_cast<int>((random * i) / max_iterations)];
							break;
						case 2:
							c = hot_colormap[static_cast<int>((random * i) / max_iterations)];
							break;
						case 3:
							c = yarg_colormap[static_cast<int>((random * i) / max_iterations)];
							break;
						case 4:
							c = gray_colormap[static_cast<int>((random * i) / max_iterations)];
							break;
						case 5:
							c = vga_colormap[static_cast<int>((random * i) / max_iterations)];
							break;
						default:
							break;
						}
						

						fractal.set_pixel(x, y, c);

						break;
					}
				}
			}
		}

		fractal.save_image(file);

		return 7;
	}
}

